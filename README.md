# SCRUM APPROACH FOR CONCEPTION AND IMPLEMENTATION OF SOLUTION USING RSA ALGORITHM

# TEAM MEMBERS :


#   NAME ET SURNAME                               MATRICULE                       ROLES
* BAKANG EKOSSO SALMA RIHANA                        17T2229                         DESIGNER & TESTING
* DJIEMBOU TIENTCHEU VICTOR NICO                    17T2051                         SCRUM MASTER
* DJOKO NIE MARC KEVIN                              17E2424                         DESIGNER & DEVELOPPER 
* KENFACK TEMGOUA VANESSA                           17J2871                         ANALYST & DEVELOPPER & TESTING
* MOUNTAPMBEME NJAPNDOUNKE HELENE NADIA             17T2085                         DEVELOPPER & TESTING

# PROJECT INFORMATIONS
Ce projet est produit dans le concept des examens travaux pratiques de l'unite d'enseignement INFO 325 prouvant que les etudiant on acquis les informations voulu <Les bases de la securite informatique et la gestion des risques>.
La thematique sur laquelle notre groupe travail est <RSA Algorithm>. 
Il sera donc question ici de pouvoir 
*  d'etudier et comprendre l'algorithme RSA
*  Ecrire un programme base sur cette algorithme pour resoudre un probleme precis.


# PROBLEME CIBLE
<Confidentialite> Dans la vie de tous les jours il est difficile d'assurer la confidentialise fiable dans nos systeme utilise. Nous allons donc nous attaquer a un moyen d'assurer la confidentialite de ressources (texte, fichier, archive).

# PROJECT STRUCTURE


# ITERATIONS

* [ ] IT1
* [ ] IT2
* [ ] IT3

# REVIEW 

* [ ] R1
* [ ] R2
* [ ] Rn


